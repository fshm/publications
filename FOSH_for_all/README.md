# FOSH for all

## LICENSE

CC BY-SA 4.0

## Languages

Tamil & English

## Contributing

Usual git rebase is not possible, we have to work using Merge Requests.

Try not work directly on English version, as it is the master copy undergoind rapid changes.

Try to only translate the English version into Tamil

## File Format

As recommended by LibreOffice,
https://wiki.documentfoundation.org/Libreoffice_and_subversion

LibreOffice Draw Flat XML - .fodg
LibreOffice Writer Flat XML - .fodt

## Status 

| Topic | EN content | TA content | EN typesetting | TA typesetting |
| --- | --- | --- | --- | --- |
| Foreword | Working | XXX | XXX | XXX | 
| Introduction to FOSH | Completed | XXX | XXX | XXX 
| Impact of FOSH | Working | XXX | XXX | XXX |
